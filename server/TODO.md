# TODO

- make a more powerful configuration file/directory merge algorithm

	Current merge algorithm only merges "`main.json`" with directory tree contents.
	It is not possible to have a "`db.json`" and a "`db/`" folder to be merged.

- make configuration merging depend on environment

	Current merge algorithm doesn’t handle environment.
	It should merge trees (both defaults and user) based on current environment.
