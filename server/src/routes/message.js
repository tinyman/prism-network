'use strict';

import Mioga from '../core/Mioga';
import message from '../objects/message';
import utilisateur from '../objects/utilisateur'

module.exports = {
	'/': {
		// Get all messages
		get: function(req, res, next) {
			var listeMessage = new message(req.mioga);
			listeMessage.load()
				.then(() => {
					res.send(listeMessage.get());
				});
		},

		// Create a message
		post: function(req, res, next) {
			var unMessage = new message(req.mioga, req.body);
			unMessage.store();
			res.send(unMessage.get());
		}
	},

	'/:id': {
		// Get a message
		get: function(req, res, next) {
			var unMessage = new message(req.mioga, {
				id: req.params.id
			});
			unMessage.load()
				.then(() => {
					var data = unMessage.get();
					var idUser = data.utilisateur;
					var a_user = new utilisateur(req.mioga,{
						id: idUser
					});

					a_user.load()
					.then(() => {
						var userData = a_user.get();
						data.utilisateur = userData.prenom + " " + userData.nom
						res.send(data);
					})
				});
		},

		// Update a message
		put: function(req, res, next) {
			var unMessage = new message(req.mioga, {
				id: req.params.id
			});
			unMessage.load()
				.then(() => {
					for (var attr in req.body) {
						unMessage[attr] = req.body[attr];
					}
					unMessage.store();
					res.send(unMessage.get());
				});
		},

		// Delete a message
		delete: function(req, res, next) {
			var unMessage = new message(req.mioga, {
				id: req.params.id
			});
			unMessage.delete()
				.then(response => {
					res.send(response);
				});
		}
	}
}
