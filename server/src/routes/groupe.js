'use strict';

import Mioga from '../core/Mioga';
import groupe from '../objects/groupe';
import messageGroupe from '../objects/messageGroupe';
import message from '../objects/message';
import utilisateurGroupe from '../objects/utilisateurGroupe';

module.exports = {
    '/': {
		//Get all groups		
        get: function(req, res, next){
            var lesGroupes = new groupe(req.mioga);
            lesGroupes.load()
                .then(() => {
                    res.send(lesGroupes.get());
                })
        },

        // Create a group
		post: function(req, res, next) {
			var unGroupe = new groupe(req.mioga, req.body);
			unGroupe.store();
			res.send(unGroupe.get());
		}
    },

    '/:id': {
        //Get a group
        get: function(req, res, next) {
            var unGroupe = new groupe(req.mioga,{
                id: req.params.id
            });
            unGroupe.load()
                .then(() => {
                    res.send(unGroupe.get());
                });
        },

        // Update a group
		put: function(req, res, next) {
			var unGroupe = new utilisateur(req.mioga, {
				id: req.params.id
			});
			unGroupe.load()
				.then(() => {
					for (var attr in req.body) {
						unGroupe[attr] = req.body[attr];
					}
					unGroupe.store();
					res.send(unGroupe.get());
				});
		},

    },
    '/:id/messages': {
		// Get group messages
		get: function(req, res, next) {
			var listeMessage = new message(req.mioga);
			listeMessage.load()
				.then(() => {
					console.log("ok")
					listeMessage.getMessageByGroupId(req.params.id)
					.then(data =>{
						res.send(data);
					})
					
					// renvoyer la liste tout de suite
				});
		}
	},
	'/:id/utilisateurs': {
		// Get group utilisateurs
		get: function(req, res, next) {			
			var utilisateursGroupe = new utilisateurGroupe(req.mioga,{
				groupe: req.params.id
			});
			utilisateursGroupe.load()
				.then(() => {
					res.send(utilisateursGroupe.get());
					// renvoyer la liste tout de suite
				});
		}
	}
}