'use strict';

import Mioga from '../core/Mioga';
import messageGroupe from '../objects/messageGroupe';

module.exports = {
	'/': {
		// Get all messageGroupes
		get: function(req, res, next) {
			var listeMessageGroupe = new messageGroupe(req.mioga);
			listeMessageGroupe.load()
				.then(() => {
					res.send(listeMessageGroupe.get());
				});
		}
	}
}
