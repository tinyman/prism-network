'use strict';

import Mioga from '../core/Mioga';
import utilisateur from '../objects/utilisateur';
import message from '../objects/message';

module.exports = {
	'/': {
		// Get all users
		get: function(req, res, next) {
			var users = new utilisateur(req.mioga);
			users.load()
				.then(() => {
					res.send(users.get());
				});
		},

		// Create a user
		post: function(req, res, next) {
			var user = new utilisateur(req.mioga, req.body);
			user.store();
			res.send(user.get());
		}
	},

	'/:id': {
		// Get a user
		get: function(req, res, next) {
			var util = new utilisateur(req.mioga,{
				id: req.params.id
			});
			util.load()
				.then(() => {
					res.send(util.get());
				});
		},

		// Update a user
		put: function(req, res, next) {
			var user = new utilisateur(req.mioga, {
				id: req.params.id
			});
			user.load()
				.then(() => {
					for (var attr in req.body) {
						user[attr] = req.body[attr];
					}
					user.store();
					res.send(user.get());
				});
		},

		// Delete a user
		delete: function(req, res, next) {
			var user = new utilisateur(req.mioga, {
				id: req.params.id
			});
			user.delete()
				.then(response => {
					res.send(response);
				});
		}
	},

	'/:id/messages': {
		// Get user messages
		get: function(req, res, next) {
			var messagesUtilisateur = new message(req.mioga,{
				utilisateur: req.params.id
			});
			messagesUtilisateur.load()
				.then(() => {
					res.send(messagesUtilisateur.get());
				});
		}
	}
}
