'use strict';

import Mioga from '../core/Mioga';
import messageGroupe from '../objects/utilisateurGroupe';

module.exports = {
	'/': {
		// Get all messageGroupes
		get: function(req, res, next) {
			var listeUtilisateurGroupe = new utilisateurGroupe(req.mioga);
			listeUtilisateurGroupe.load()
				.then(() => {
					res.send(listeUtilisateurGroupe.get());
				});
		}
	}
}
