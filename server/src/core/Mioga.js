/************************************************************
 *
 * Mioga
 *
 *    This is the main Mioga object. Its purpose is to pass
 *    common resources to the different objects.
 *
 ************************************************************/

'use strict';

/**
 * Module dependencies
 */
var merge = require('merge');
var fs = require('fs');
var requireDirectory = require('require-directory');
import Logger from './Logger';



export default class Mioga {
	constructor() {
		this._loadConfig();
		this._log = new Logger(this, 'Mioga');
		this._loadDb();
	}


	/**
	 * Get a configuration key value
	 */
	get(key) {
		/**
		 * Recursive config inspector
		 * From https://github.com/harishanchu/nodejs-config
		 */
		var getImpl = function(object, property) {
			var t = this,
				elems = Array.isArray(property) ? property : property.split('.'),
				name = elems[0],
				value = object[name];
			if (elems.length <= 1) {
				return value;
			}
			// Note that typeof null === 'object'
			if (value === null || typeof value !== 'object') {
				return undefined;
			}
			return getImpl(value, elems.slice(1));
		};
		return getImpl(this.config, key);
	}


	/**
	 * Get DB access object
	 */
	get db() {
		return this._db;
	}


	/**
	 * Load configuration files
	 */
	_loadConfig() {
		/**
		 * Load default settings
		 */
		var defaults = merge.recursive(
				require('../../config/defaults/main.json'),
				requireDirectory(module, '../../config/defaults/')
			);
		delete(defaults.main);


		/**
		 * Load user settings
		 */
		var user;
		try {
			user = require('../../config/user.json');
		}
		catch (ex) {
			user = {};
		}


		/**
		 * Merge defaults & user settings
		 */
		this.config = merge.recursive(
				defaults,
				user
			);
	}


	/**
	 * Load database driver
	 */
	_loadDb() {
		try {
			var DbDriver = require(`./db/${this.get('db.type')}.js`);
			this._db = new DbDriver(this);
		}
		catch(e) {
			this._log.error(`Failed to load database driver: ${this.get('db.type')}`);
			this._log.error(e);
		}
	}
}

