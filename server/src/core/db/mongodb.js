'use strict';
import Logger from '../Logger';

export default class MongoDriver {
	constructor(mioga) {
		var _this = this;
		this._log = new Logger(mioga, 'MongoDB');
		this._log.trace("Entering");

		// Load module
		this._mongoose = require('mongoose');

		// Set mongoose.Promise to native ES6 Promise implementation
		this._mongoose.Promise = Promise;

		// Connect
		this._uri = mioga.get('db.mongodb.url');
		this._log.debug(`Connecting to ${this._uri}`);
		this._mongoose.connect(this._uri);

		// connection events
		this._mongoose.connection.on('connected', () => {
			_this._log.info(`Mongoose connected to ${_this._uri}`);
		});

		this._mongoose.connection.on('error', (err) => {
			_this._log.error(`Mongoose connection error: ${err}`);
		});

		this._mongoose.connection.on('disconnected', () => {
			_this._log.info('Mongoose disconnected');
		});

		this._mongoose.connection.once('open', (err, data) => {
			_this._log.debug('Mongo working!');
		});

		// for nodemon restarts
		process.once('SIGUSR2', () => {
			_this.gracefulShutdown('nodemon restart', () => {
				process.kill(process.pid, 'SIGUSR2');
			});
		});

		// for app termination
		process.on('SIGINT', () => {
			_this.gracefulShutdown('app termination', () => {
				process.exit(0);
			});
		});

		this._log.trace("Leaving");

		return this._mongoose;
	}

	// capture app termination / restart events
	// To be called when process is restarted or terminated
	gracefulShutdown(msg, cb) {
		var _this = this;
		this._mongoose.connection.close(() => {
			_this._log.info(`Mongoose disconnected through ${msg}`);
			cb();
		});
	}
}
