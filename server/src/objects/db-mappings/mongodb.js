/************************************************************
 *
 * MongoDB common bindings
 *
 *     These methods are available to all objects when using
 *     MongoDB
 *
 ************************************************************/

'use strict';

module.exports = {
	load: function() {
		this.log.trace('[load] Entering');

		// Translate "id" to "_id"
		if (this._attr.id) {
			this._attr._id = this._attr.id;
			delete(this._attr.id);
		}

		// Query and return result
		return this._model.find(this._attr)
			.exec()
			.then(found => {
				if(found.length === 1) {
					// Query returned single record
					this.log.trace(`[load] Single record found in DB, id ${found._id}`);
					this._attr = found[0].toJSON();
				}
				else if(found.length > 1) {
					// Query returned multiple records
					this.log.trace(`[load] ${found.length} records found in DB`);
					this._list = [];
					found.forEach(record => {
						this._list.push(record.toJSON());
					});
				}
				else {
					// Query returned no record
					this._attr = {};
					this._list = [];
				}
				this.log.trace('[load] Leaving');
			});
	},

	store: function() {
		this.log.trace('[store] Entering ');

		// Translate "_id" to "id"
		if (this._attr._id) {
			this._attr.id = this._attr._id;
			delete(this._attr._id);
		}

		if(!this._attr.id) {
			// Create record
			this.log.trace('[store] Creating new record');
			var u = new this._model(this._attr);
			return u.save()
					.then(() => {
						this.log.trace('[store] Record created successfully');
						this.log.trace('[store] Leaving');
					})
					.catch(error => {
						this.log.error('[store] Record creation failed');
						this.log.error(error);
						this.log.trace('[store] Leaving');
					});
		}
		else {
			// Update record
			this.log.trace(`[store] Updating existing record ${this._attr.id}`);
			return this._model.update({_id: this._attr.id}, this._attr)
					.then(records => {
						this.log.trace('[store] Record updated successfully');
						this.log.trace('[store] Leaving');
					})
					.catch(error => {
						this.log.error('[store] Record update failed');
						this.log.error(error);
						this.log.trace('[store] Leaving');
					});
		}

		this.log.trace('[store] Leaving');
	},

	delete: function() {
		this.log.trace('[delete] Entering');

		// Translate "id" to "_id"
		if (this._attr.id) {
			this._attr._id = this._attr.id;
			delete(this._attr.id);
		}

		return this._model.find(this._attr)
			.remove()
			.exec();

		this.log.trace('[delete] Leaving');
	}
};
