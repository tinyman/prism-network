/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');
		this._model = this._db.define('message_groupe', {
			message: {
				type: Sequelize.INTEGER,
				primaryKey: true
			},
			groupe: {
				type: Sequelize.INTEGER,
				primaryKey: true
			},
			visible: Sequelize.BOOLEAN,
			plus: Sequelize.INTEGER,
			moins: Sequelize.INTEGER
		});

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	},

	/*test: function(){
		this._db.query("select * from message_groupes mg left join messages m on mg.message = m.id ")
	}*/
};
