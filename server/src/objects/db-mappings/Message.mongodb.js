/************************************************************
 *
 * Message.mongodb
 *
 *    Message-specific MongoDB bindings.
 *
 ************************************************************/

'use strict';

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		var schema = this._db.Schema({
			messageIntitule: String,
			messageUser: String,
			messageGroupe: Array
		});

		this._model = this._db.models.message || this._db.model('message', schema);

		this.log.trace('[schema] Leaving');
	}
};
