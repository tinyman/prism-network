/************************************************************
 *
 * Group.mongodb
 *
 *    Group-specific MongoDB bindings.
 *
 ************************************************************/

'use strict';

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		var schema = this._db.Schema({
			nomGroupe: String,
			groupeUsers: Array
		});

		this._model = this._db.models.group || this._db.model('group', schema);

		this.log.trace('[schema] Leaving');
	}
};
