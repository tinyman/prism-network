/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		this._model = this._db.define('groupe', {
			id: {
				type: Sequelize.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},
			nom: Sequelize.STRING,
		});

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
