/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class utilisateur extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		this.log.trace("[init] Leaving");
	}

	get id() { return this._attr.id };
	set id(val) { this._attr.id = val};
	
	get mail() { return this._attr.mail };
	set mail(val) { this._attr.mail = val};
	
	get password() { return this._attr.password };
	set password(val) { this._attr.password = val};
	
	get nom() { return this._attr.nom };
	set nom(val) { this._attr.nom = val};

	get prenom() { return this._attr.prenom };
	set prenom(val) { this._attr.prenom = val};
}

export default utilisateur;
