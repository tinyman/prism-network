/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class message extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		this.log.trace("[init] Leaving");
	}

	get id() { return this._attr.id };
	set id(val) { this._attr.id = val};
	
	get contenu() { return this._attr.contenu };
	set contenu(val) { this._attr.contenu = val};
	
	get dateCreation() { return this._attr.dateCreation };
	set dateCreation(val) { this._attr.dateCreation = val};
	
	get dateModif() { return this._attr.dateModif };
	set dateModif(val) { this._attr.dateModif = val};

	getMessageByGroupId(id){
		this._db.query("select mg.groupe, mg.message, m.contenu, m.createdAt, m.updatedAt, u.id as idUser, u.nom, u.prenom from message_groupes mg left join messages m on mg.message = m.id left join utilisateurs u on m.utilisateur = u.id where mg.groupe = ? ", { replacements: [id], type: this._db.QueryTypes.SELECT })
		.then(messages => {
			return messages
		})
	}

	// utilisateur.findAll({
	//	include: [{
	//	  model: utilisateur_groupe
	//	}]
	//});

}

export default message;
