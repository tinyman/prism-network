/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class utilisateurGroupe extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		this.log.trace("[init] Leaving");
	}

	get utilisateur() { return this._attr.utilisateur };
	set utilisateur(val) { this._attr.utilisateur = val};
	
	get groupe() { return this._attr.groupe };
	set groupe(val) { this._attr.groupe = val};
	
	get admin() { return this._attr.admin };
	set admin(val) { this._attr.admin = val};
}

export default utilisateurGroupe;
