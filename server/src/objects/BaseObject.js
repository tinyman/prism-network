/************************************************************
 *
 * BaseObject
 *
 *    This is the base for all other classes that should
 *    extend it.
 *
 ************************************************************/

'use strict';
// 	adapter le constructeur pour opérateur IN --> sequelize
import Logger from '../core/Logger';

class BaseObject {
	_attr = {};
	_list = [];

	constructor(mioga, attr) {
		// Mioga commons
		this._config = mioga;
		this._db = mioga.db;

		// Logging system
		this.log = new Logger(mioga, this.constructor.name);

		// Object attributes
		for (var key in attr) {
			this._attr[key] = attr[key];
		}

		// Load database-specific DB bindings
		var DBBindings = require(`./db-mappings/${this._config.get("db.type")}`);
		for (var key in DBBindings) {
			if (DBBindings.hasOwnProperty(key)) this[key] = DBBindings[key];
		}

		// Load object-specific DB bindings
		var ObjectBindings = require(`./db-mappings/${this.constructor.name}.${this._config.get("db.type")}`);
		for (var key in ObjectBindings) {
			if (ObjectBindings.hasOwnProperty(key)) this[key] = ObjectBindings[key];
		}
		// Load object's schema
		if (this.schema)
			this.schema();

		// Specific initialization
		// WARNING: this should remain at the end of the constructor
		this.init();
	}

	get() {
		if(this._list.length > 0)
			return this._list;
		else
			return this._attr;
	}
	
	// To be overloaded
	init() {}
}

module.exports = BaseObject;
