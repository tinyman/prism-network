
'use strict';

import BaseObject from './BaseObject';

class groupe extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		this.log.trace("[init] Leaving");
    }
    
	get id() { return this._attr.id };
    set id(val) { this._attr.id = val};

    get nom() { return this._attr.nom };
    set nom(val) { this._attr.nom = val};   

}

export default groupe;
