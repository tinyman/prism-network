/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class messageGroupe extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		this.log.trace("[init] Leaving");
	}

	get message() { return this._attr.message };
	set message(val) { this._attr.message = val};
	
	get groupe() { return this._attr.groupe };
	set groupe(val) { this._attr.groupe = val};
	
	get visible() { return this._attr.visible };
	set visible(val) { this._attr.visible = val};
	
	get plus() { return this._attr.plus };
	set plus(val) { this._attr.plus = val};

	get moins() { return this._attr.moins };
	set moins(val) { this._attr.moins = val};
}

export default messageGroupe;
