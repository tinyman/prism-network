/************************************************************
 *
 *    This is the main Mioga startup script.
 *
 ************************************************************/

'use strict';

import Mioga from './core/Mioga';
import Logger from './core/Logger';
import Router from './core/Router';


/**
 * Load Mioga-specific components
 */
var mioga = new Mioga();
var log = new Logger(mioga, 'Server');

log.info('Mioga3 is starting up');


/**
 * Load ExpressJS components
 */
var express = require('express');
var http = require('http');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var helmet = require('helmet');
var methodOverride = require('method-override');
var paginate = require('express-paginate');
var cors = require('cors');
var expressValidator = require('express-validator');
var morgan = require('morgan')


/**
 * Initialize ExpressJS app
 */
var app = express();
var server = http.Server(app);


/**
 * Configure middleware
 */
app.use(cors());
app.use(compression());
app.use(helmet());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(cookieParser());
app.use(paginate.middleware(10, 100));
app.use(morgan(mioga.get('log.format')));


/**
 * Configure client tree access
 */
app.use(require('express-markdown')({
	directory: mioga.get('client.path')
}));
app.use(express.static(mioga.get('client.path')));


/**
 * Load routes
 */
var router = new Router(mioga, app);

/**
 * Initialize sqlite db
 */
const sqlite3 = require('sqlite3').verbose();
var fs = require('fs');
var filePath = 'data/db.sqlite3';
if (fs.existsSync(filePath)) {
	fs.unlinkSync(filePath);
}
const db = new sqlite3.Database(`data/db.sqlite3`);
log.info('Initializing db ...');
db.serialize(function() {
	db.run("CREATE TABLE utilisateurs (`id` UUIDV1 PRIMARY KEY, `mail` VARCHAR(255), `password` VARCHAR(255), `nom` VARCHAR(255), `prenom` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL)");
	db.run("CREATE TABLE groupes (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `nom` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL)");
	db.run("CREATE TABLE messages (`id` UUIDV1 PRIMARY KEY, `utilisateur` INTEGER, `contenu` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL)");
	db.run("CREATE TABLE message_groupes (`message` INTEGER NOT NULL, `groupe` INTEGER NOT NULL, `visible` TINYINT(1), `plus` INTEGER, `moins` INTEGER, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, PRIMARY KEY (`message`, `groupe`))");
	db.run("CREATE TABLE utilisateur_groupes (`utilisateur` INTEGER NOT NULL, `groupe` INTEGER NOT NULL, `admin` TINYINT(1), `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, PRIMARY KEY (`utilisateur`, `groupe`))");

	var insertUtilisateur = "INSERT INTO utilisateurs (id,mail,password,nom,prenom,createdAt,updatedAt) VALUES (1,'utilisateur1@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur1','PrenomUtilisateur1','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,'utilisateur2@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur2','PrenomUtilisateur2','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,'utilisateur3@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur3','PrenomUtilisateur3','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,'utilisateur4@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur4','PrenomUtilisateur4','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (5,'utilisateur5@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur5','PrenomUtilisateur5','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,'utilisateur6@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur6','PrenomUtilisateur6','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (7,'utilisateur7@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur7','PrenomUtilisateur7','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (8,'utilisateur8@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur8','PrenomUtilisateur8','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (9,'utilisateur9@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur9','PrenomUtilisateur9','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (10,'utilisateur10@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur10','PrenomUtilisateur10','2018-02-14 10:20:05','2018-02-14 10:20:05')";
	db.run(insertUtilisateur);

	var insertGroupe="INSERT INTO groupes (id,nom,createdAt,updatedAt) VALUES (1,'Projet Mioga','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,'Groupe des chefs','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,'Techs','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,'Club de Foot ','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (5,'HelpDesk','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,'Dev Interne','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (7,'Dev Externe','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (8,'Bénévoles de la Salle de Sport','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (9,'Projet Portail','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (10,'Recherche et Developpement','2018-02-14 10:20:05','2018-02-14 10:20:05')";
	db.run(insertGroupe);
	
	var insertMessage = "INSERT INTO messages (id,utilisateur,contenu,createdAt,updatedAt) VALUES (1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,2,'oui uio ui oui oui ou ioui ou ioui oui oui oui oui oui ouis laborum.','2018-02-14 10:21:05','2018-02-14 10:20:05')"+
	", (3,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (5,5,'oui uio ui oui oui ou ioui ou ioui oui oui oui oui oui oui ','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (7,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (8,8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (9,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (10,10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (11,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (12,2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (13,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (14,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (15,5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (16,6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (17,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (18,8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (19,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (20,10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05','2018-02-14 10:20:05')";
	db.run(insertMessage);

	var insertUtilisateurGroupe = "INSERT INTO `utilisateur_groupes` (utilisateur,groupe,admin,createdAt,updatedAt) VALUES (1,1,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (1,2,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (1,5,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,1,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,7,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,1,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,4,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,1,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,2,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (5,5,1,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,2,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,4,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,5,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (7,5,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (8,2,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (9,7,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (10,7,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')";
	db.run(insertUtilisateurGroupe);
	
	var insertMessageGroupe="INSERT INTO `message_groupes` (message,groupe,visible,plus,moins,createdAt,updatedAt) VALUES (1,1,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (1,2,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,7,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (2,1,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,4,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (3,1,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (4,1,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,2,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (6,5,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (7,5,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (8,2,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (9,7,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (10,7,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (11,5,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (11,2,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (12,7,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (12,1,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (13,3,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (13,1,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (14,2,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (14,1,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (16,4,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (16,2,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (16,5,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (17,5,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (18,2,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (19,7,0,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (20,7,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (5,5,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')"+
	", (15,5,1,0,0,'2018-02-14 10:20:05','2018-02-14 10:20:05')";
	db.run(insertMessageGroupe);
});

/**
 * Start server
 */
var port = mioga.get('api.port');
server.listen(port, () => {
		log.info('Welcome Mioga3!');
		log.info(`Server is listening http:\/\/localhost:${port}`);
});