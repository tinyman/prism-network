# Projet 
Projet tutoré LP PRISM Orsay 2017-2018

## SQLite

la base de donnée se recrée à chaque `npm start`

Exemple des requetes :

    //GET
    // Select *
    http://localhost:3000/utilisateur
    http://localhost:3000/message
    http://localhost:3000/groupe
    
    // Select where id=1
    http://localhost:3000/utilisateur/1
    http://localhost:3000/message/1
    http://localhost:3000/groupe/1
    
    http://localhost:3000/groupe/1/messages // liste des messages du groupe ou id = 1
    
    //POST 
    // Insert Into ...
    http://localhost:3000/groupe // Avec les paramètres dans le body en x-www-form-urlencoded 
    
    ... et ainsi de suite vous pouvez voir dans les fichier de routes les méthodes disponibles (crud classique pour l'instant)

[MCD](https://gitlab.com/tinyman/prism-network/raw/53f121d2a608657d2c8ef091ccfc5e85a38b07c4/scripts/sqlite/mioga.PNG)

**Pour parcourir la bd** on vous conseille DB Browser for SQLite : http://sqlitebrowser.org/

**Les password des utilisateurs sont tous "12345" hashé en SHA1**
        
