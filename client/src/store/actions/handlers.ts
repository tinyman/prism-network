import { State } from '@/store/state';
import { Store, ActionTree, ActionContext } from 'vuex';
import {
  LoginMutation,
  LoginSuccessMutation,
  LoadMessageSuccessMutation,
  LoadMessagesMutation,
  SelectGroupMutation,
  LoadGroupsSuccessMutation,
  AddMessageMutation,
  CreateGroupMutation,
  CreateGroupSuccessMutation,
} from '@/store/mutations';
import {
  LoginAction,
  LoadMessagesAction,
  SelectGroupAction,
  SELECT_GROUP,
  LOGIN,
  LOAD_MESSAGES,
  LoadGroupsAction,
  LOAD_GROUPS,
  SUBMIT_MESSAGE,
  SubmitMessageAction,
  CREATE_GROUP,
  CreateGroupAction,
} from './actions';
import { getGroups, getMessages, submitMessage, createGroup } from '@/api';

async function delay(x = 1000) {
  return await new Promise((res) => setTimeout(res, x));
}

type Context = ActionContext<State, State>;

export const actions = {
  async [LOGIN]({ commit, dispatch }: Context, { payload: { login, password } }: LoginAction): Promise<void> {
    console.log('Login action ', login, password);
    commit(new LoginMutation());
    await delay();
    commit(new LoginSuccessMutation(login));
  },
  async [LOAD_MESSAGES]({ commit }: Context, { payload: { groupId } }: LoadMessagesAction) {
    commit(new LoadMessagesMutation());
    const messages = await getMessages(groupId);
    commit(new LoadMessageSuccessMutation(messages, parseInt(groupId,10)));

  },
  async [SELECT_GROUP]({ commit, dispatch }: Context, { id }: SelectGroupAction) {
    commit(new SelectGroupMutation(parseInt(id,10)));
    await dispatch(new LoadMessagesAction({ groupId: id }));
  },
  async [LOAD_GROUPS]({ commit }: Context, action: LoadGroupsAction) {
    const groups = await getGroups();
    commit(new LoadGroupsSuccessMutation(groups));
  },
  async [SUBMIT_MESSAGE]({ commit, getters }: Context, { payload: { message, groupIds } }: SubmitMessageAction) {
    if (message.message.trim().length === 0) { return; }
    message = await submitMessage({ message, groupIds });
    groupIds.forEach((groupId) => commit(new AddMessageMutation({
      message,
      groupId : parseInt( groupId,10),
    })));
  },
  async [CREATE_GROUP]({ commit, getters }: Context, { payload }: CreateGroupAction) {
    commit(new CreateGroupMutation({ name: payload.name }));
    const group = await createGroup(payload.name);
    commit(new CreateGroupSuccessMutation(group));
  },
};
