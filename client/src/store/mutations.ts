import { State } from '@/store/state';
import { Message } from '@/models/Message';
import { Group } from '@/models/Group';

export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOAD_MESSAGES = 'LOAD_MESSAGES';
export const LOAD_MESSAGES_FAIL = 'LOAD_MESSAGES_FAIL';
export const LOAD_MESSAGES_SUCCESS = 'LOAD_MESSAGES_SUCCESS';
export const SELECT_GROUP = 'SELECT_GROUP';
export const LOAD_GROUPS_SUCCESS = 'LOAD_GROUP_SUCCESS';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CREATE_GROUP = 'CREATE_GROUP';
export const CREATE_GROUP_SUCCESS = 'CREATE_GROUP_SUCCESS';

export class LoginMutation {
  public readonly type = LOGIN;
}
export class LoginFailMutation {
  public readonly type = LOGIN_FAIL;
}
export class LoginSuccessMutation {
  public readonly type = LOGIN_SUCCESS;
  constructor(public readonly login:string){}
}
export class LoadMessagesMutation {
  public readonly type = LOAD_MESSAGES;
}
export class LoadMessagesFailMutation {
  public readonly type = LOAD_MESSAGES_FAIL;
}
export class LoadMessageSuccessMutation {
  public readonly type = LOAD_MESSAGES_SUCCESS;
  constructor(public readonly messages: Message[], public readonly groupId: number) { }
}
export class SelectGroupMutation {
  public readonly type = SELECT_GROUP;
  constructor(public readonly id: number) { }
}
export class LoadGroupsSuccessMutation {
  public readonly type = LOAD_GROUPS_SUCCESS;
  constructor(public readonly groups: Group[]) { }
}
export class AddMessageMutation {
  public readonly type = ADD_MESSAGE;
  constructor(public readonly payload: {
    message: Message,
    groupId: number,
  }) { }
}

export class CreateGroupMutation {
  public readonly type = CREATE_GROUP;
  constructor(public readonly payload: { name: string }) { }
}
export class CreateGroupSuccessMutation {
  public readonly type = CREATE_GROUP_SUCCESS;
  constructor(public readonly group: Group) { }
}

export const mutations = {
  [LOGIN](state: State) {
    state.pending = true;
  },
  [LOGIN_SUCCESS](state: State, login:string) {
    state.isLoggedIn = true;
    state.pending = false;
    state.user = { id: 1 , name:login}
  },
  [LOGIN_FAIL](state: State) {
    state.isLoggedIn = false;
    state.pending = false;
  },
  [LOAD_MESSAGES](state: State) {
    state.pending = true;
  },
  [LOAD_MESSAGES_SUCCESS](state: State, payload: LoadMessageSuccessMutation) {
    const group = state.groups.find((g) => g.id === payload.groupId);
    console.dir(payload);
    if (group) { group.messages = payload.messages; }
  },
  [LOAD_MESSAGES_FAIL](state: State) {
    state.pending = false;
  },
  [SELECT_GROUP](state: State, payload: SelectGroupMutation) {
    console.dir(payload);
    state.currentGroup = state.groups.find((g) => g.id === payload.id) || null;
  },
  [LOAD_GROUPS_SUCCESS](state: State, { groups }: LoadGroupsSuccessMutation) {
    state.groups = groups;
    state.pending = false;
  },
  [ADD_MESSAGE](state: State, { payload: { message, groupId } }: AddMessageMutation) {
    const group = state.groups.find((g) => g.id === groupId);
    if (group) {
      group.messages.unshift(message);
    }
  },
  [CREATE_GROUP](state: State, { payload: { name } }: CreateGroupMutation) {
    // loading
  },
  [CREATE_GROUP_SUCCESS](state: State, { group }: CreateGroupSuccessMutation) {
    state.groups.push(group);
  },

};
