import { Group } from '@/models/Group';
import { groups, messages } from '@/api/mock';
import { Message } from '@/models/Message';
import axios from 'axios';

export const baseUrl = 'http://localhost:3000';

export async function getGroups(): Promise<Group[]> {
  let groupsGetted: Group[] = [];
  try {

    let response = await axios.get(baseUrl + '/groupe/');

    response.data.forEach(function (value: any) {
      let newGroup: Group = { id: value.id, name: value.nom, messages: [] };
      groupsGetted.push(newGroup);

    });

    return groupsGetted;
  } catch (error) {
    console.log(error);
  }

  return groupsGetted;
}

export async function getGroup(groupId: string): Promise<Group | null> {


  try {

    let response = await axios.get(baseUrl + '/groupe/' + groupId);
    let newGroup: Group = { id: response.data.id, name: response.data.nom, messages: [] };
    return newGroup;
  } catch (error) {
    console.log(error);
  }

  return null;

  /* const group = groups.find((g) => g.id === groupId);
   if (group) {
     return Promise.resolve(group);
   } else { return Promise.reject(new Error('Not found')); }*/
}

export async function getMessages(groupId: string): Promise<Message[]> {

  let msgsGetted: Message[] = [];
  try {

    let response = await axios.get(baseUrl + '/groupe/' + groupId + "/messages");
    let requests = response.data.map(function (value: any) {
      return axios.get(baseUrl + "/message/" + value.message)
        .then(function (response) {
          let message: Message = { id: response.data.id, message: response.data.contenu, date: response.data.createdAt }
          msgsGetted.push(message);
          console.log(message);
        })
        .catch(function (error) {
          console.log(error);
        });
    });
    //groups[groupId].messages = response.data
    await Promise.all(requests)
    return msgsGetted;

  } catch (error) {
    console.log(error);
  }
  return msgsGetted;
}

export function submitMessage({ message, groupIds }: { message: Message, groupIds: string[] }): Promise<Message> {

  message.id = Math.random().toString();
  axios.post(baseUrl + "/message", {

  })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  return Promise.resolve(message);
}

export function createGroup(name: string): Promise<Group> {

  return Promise.resolve({
    id: Math.random(),
    name,
    messages: [],
  });
}
