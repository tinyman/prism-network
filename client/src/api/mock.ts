import { Group } from '@/models/Group';
import { Message } from '@/models/Message';

export const groups: Group[] = [];
export const messages: { [key: string]: Message[] } = {
  group1: [
    {
      id: 'msg4', message: `$$f(x) = \\int_{-\\infty}^\\infty
      \\hat f(\\xi)\\,e^{2 \\pi i \\xi x}
      \\,d\\xi$$`, date: new Date(),
    },
    {
      id: 'msg3', message: `\`\`\`javascript
const test = '123';

function bonjour() {
  var a = Object.assign({}, { hey: ''});
  return 1 + (() => 2)();
}\n\`\`\``, date: new Date(),
    },
    { id: 'msg2', message: 'Bonjour', date: new Date() },
    { id: 'msg1', message: 'Hello', date: new Date() },
  ],
  group2: [
    { id: 'msg2', message: 'Hi', date: new Date() },
  ],
};
