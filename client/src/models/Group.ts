import { Message } from '@/models/Message';

export interface Group {
  id: number;
  name: string;
  messages: Message[];
}
