import { User } from "@/models/User";

export interface Message {
  id?: string;
  message: string;
  date: Date;
  user : User;
}
